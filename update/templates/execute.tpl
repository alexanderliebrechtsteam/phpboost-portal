	<header>
		<h2>{@step.execute.message}</h2>
	</header>
	
	<div class="content">
		{@H|step.execute.explanation}
	</div>
	
	<footer>
		<div class="next-step">
			# INCLUDE SERVER_FORM #
		</div>
	</footer>

