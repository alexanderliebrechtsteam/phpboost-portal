<?php
/*##################################################
 *                           common.php
 *                            -------------------
 *   begin                : 12 August, 2014
 *   copyright            : (C) 2014 Kevin MASSY
 *   email                : kevin.massy@phpboost.com
 *
 *
 ###################################################
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 ###################################################*/

 ####################################################
 #                     French                       #
 ####################################################
 
$lang['bb_smileys'] = 'Smileys';
$lang['bb_bold'] = 'Texte en gras : [b]texte[/b]';
$lang['bb_italic'] = 'Texte en italique : [i]texte[/i]';
$lang['bb_underline'] = 'Texte souligné : [u]texte[/u]';
$lang['bb_strike'] = 'Texte barré : [s]texte[/s]';
$lang['bb_link'] = 'Ajouter un lien : [url]lien[/url], ou [url=lien]nom du lien[/url]';
$lang['bb_picture'] = 'Ajouter une image : [img]url image[/img]';
$lang['bb_lightbox'] = 'Ajouter une image avec un effet lightbox : [lightbox=url image][img]url image réduite[/img][/lightbox]';
$lang['bb_size'] = 'Taille du texte (X entre 0 - 49) : [size=X]texte de taille X[/size]';
$lang['bb_font'] = 'Police : [font=X]texte en police X[/font]';
$lang['bb_color'] = 'Couleur du texte : [color=X]texte de couleur X[/color]';
$lang['bb_quote'] = 'Faire une citation [quote=pseudo]texte[/quote]';
$lang['bb_left'] = 'Positionner à gauche : [align=left]objet à gauche[/align]';
$lang['bb_center'] = 'Centrer : [align=center]objet centré[/align]';
$lang['bb_right'] = 'Positionner à droite : [align=right]objet à droite[/align]';
$lang['bb_justify'] = 'Justifier : [align=justify]objet justifié[/align]';
$lang['bb_code'] = 'Insérer du code [code]texte[/code]';
$lang['bb_math'] = 'Insérer du code mathématique [math]texte[/math]';
$lang['bb_swf'] = 'Insérer du flash [swf=largeur,hauteur]adresse animation[/swf]';
$lang['bb_small'] = 'Réduire le champ texte';
$lang['bb_large'] = 'Agrandir le champ texte';
$lang['bb_title'] = 'Titre [title=x]texte[/title]';
$lang['bb_html'] = 'Code html [html]code[/html]';
$lang['bb_container'] = 'Conteneur';
$lang['bb_block'] = 'Bloc';
$lang['bb_fieldset'] = 'Bloc champs';
$lang['bb_paragraph'] = 'Paragraphe';
$lang['bb_style'] = 'Style [style=x]texte[/style]';
$lang['bb_hide'] = 'Texte Caché';
$lang['bb_member'] = 'Membre';
$lang['bb_moderator'] = 'Moderateur';
$lang['bb_hide_all'] = 'Cache le texte et l\'affiche au clique [hide]text[/hide]';
$lang['bb_hide_view_member'] = 'Cache le texte pour les visiteurs [member]text[/member]';
$lang['bb_hide_view_moderator'] = 'Cache le texte pour les membres [moderator]text[/moderator]';
$lang['bb_float_left'] = 'Objet flottant à gauche [float=left]texte[/float]';
$lang['bb_float_right'] = 'Objet flottant à droite [float=right]texte[/float]';
$lang['bb_list'] = 'Liste [list][*]texte1[*]texte2[/list]';
$lang['bb_table'] = 'Tableau [table][row][col]texte[/col][col]texte2[/col][/row][/table]';
$lang['bb_indent'] = 'Indentation [indent]texte[/indent]';
$lang['bb_sup'] = 'Exposant [sup]texte[/sup]';
$lang['bb_sub'] = 'Indice [sub]texte[/sub]';
$lang['bb_anchor'] = 'Ancre vers un endroit de la page [anchor=x]texte[/anchor]';
$lang['bb_sound'] = 'Son [sound]adresse du son[/sound]';
$lang['bb_movie'] = 'Vidéo [movie=largeur,hauteur]adresse du fichier[/movie]';
$lang['bb_youtube'] = 'Ajouter une vidéo youtube : [youtube=largeur,hauteur]url youtube[/youtube]';
$lang['bb_help'] = 'Aide BBcode';
$lang['bb_upload'] = 'Attacher un fichier';
$lang['bb_url_prompt'] = 'Adresse du lien ?';
$lang['bb_anchor_prompt'] = 'Nom de l\'ancre (de la forme nom-de-l-ancre) ?';
$lang['bb_text'] = 'Texte';
$lang['bb_script'] = 'Script';
$lang['bb_web'] = 'Web';
$lang['bb_prog'] = 'Programmation';
$lang['bb_fa'] = 'Icône Font Awesome [fa]nom de l\'icône[/fa]';
$lang['bb_more'] = 'Afficher/cacher les autres fonctions';

$lang['lines'] = 'Nombre de lignes';
$lang['cols'] = 'Nombre de colonnes';
$lang['head_table'] = 'Entête';
$lang['head_add'] = 'Ajouter l\'entête';
$lang['insert_table'] = 'Insérer le tableau';
$lang['ordered_list'] = 'Liste ordonnée';
$lang['insert_list'] = 'Insérer la liste';
$lang['phpboost_languages'] = 'PHPBoost';
?>
