<section>
	<header>
		<h1>{@module.title} - {@title.table.builder}</h1>
	</header>
	<article class="content">
		<div class="pbt-box-largest">
			
			# INCLUDE table #
		</div>		
	</article>
	<footer></footer>
</section>
