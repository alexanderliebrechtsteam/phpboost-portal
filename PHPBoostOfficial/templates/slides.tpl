<li id="p-slide-{ID}" class="p-slide-element">
  <img src="{slide.U_IMG}" alt="{slide.TITLE}" class="p-slide-img" width="400" height="256"/>
  <div class="p-slide-txt">
    <a href="{slide.U_LINK}" class="p-slide-title">PHPBoost CMS</a>
    <p class="p-slide-content">
      {slide.DESC}
    </p>
    <a href="{slide.U_LINK_MORE}" class="p-slide-more" title="{slide.TITLE_MORE}">{slide.MORE}</a>
  </div>
</li>
