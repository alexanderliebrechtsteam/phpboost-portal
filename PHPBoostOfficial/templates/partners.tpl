# START partners #
<div class="partners-element" itemscope="itemscope" itemtype="http://schema.org/CreativeWork">
	<a href="{partners.U_VISIT}">
		<img src="{partners.U_PARTNER_PICTURE}" alt="{partners.NAME}" class="pbt-img pbt-img-modules" itemprop="image" />
		<p class="partners-title">{partners.NAME}</p>
	</a>
</div>
# END partners #